jQuery(document).ready(function(){
	jQuery("#contact_submit").click(function(){
		
		jQuery(".none").removeClass("error_message");
		
		var FullName = jQuery("#FullName").val();
		var Email = jQuery("#Email").val();
		var Phone = jQuery("#Phone").val();
		var yourmessage = jQuery("#yourmessage").val();
					
		var email_pattern = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;								
		if(FullName == ''){
			jQuery("#FullName").addClass("error_message");
			return false;
		}else if(Email == ''){
			jQuery("#Email").addClass("error_message");
			return false;
		}else if(Email != '' && email_pattern.test(Email) == false){
			jQuery("#Email").addClass("error_message");
			return false;
		} else if($('input[name=social]:checked').length<=0){
			jQuery("#social_error").html('Please choose your contacted: Email or Phone');
			return false;
		}else if(Phone == ''){
			jQuery("#social_error").html('');
			jQuery("#Phone").addClass("error_message");
			return false;
		} else if(yourmessage == ''){
			jQuery("#yourmessage").addClass("error_message");
			return false;
		}/* var googleResponse = jQuery('#g-recaptcha-response').val();
			
			if (!googleResponse) {
				 if (googleResponse.length == 0) {
				  $('#html_element').show();
				  return false;
				}else {
					
					if (googleResponse.length != 0) {
					  $('#html_element').hide();
					  return true;
					}
				}
			} */
			
			
			
		var captcha_response = grecaptcha.getResponse();
			if(captcha_response.length == 0)
			{
				$('#html_element').show();
				return false;
			}
			else
			{
				$('#html_element').hide();
				return true;
			}
			
	});
});