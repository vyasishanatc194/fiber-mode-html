<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<?php    
    $args = array(

    'post_type' => 'post',

    'post_status' => 'publish',

    'posts_per_page' => 1,

    );

$my_posts = get_posts( $args );
set_post_thumbnail_size( 600, 400, true );
$custom = get_post_custom(get_the_ID());
$format = "F j Y";
$pfx_date = get_the_date( $format, get_the_ID() );
?>
<div class="about_bg text-center">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="heading">
					<h1>Blog</h1>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
	<div class="blog_wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<div class="w-blog-post-h">
						<a href="<?php echo the_permalink(); ?>">
							<div class="w-blog-post-preview">
								<?php the_post_thumbnail(); ?> 
							</div>
						</a>
						<div class="w-blog-post-body">
							<h2 class="w-blog-post-title">
								<a class="entry-title"><?php echo the_title();?></a>
							</h2>
							<div class="w-blog-post-meta">
								<time class="w-blog-post-meta-date"><?php echo $pfx_date; ?></time>
								<div class="w-blog-post-content">
									<?php 
									   if ( have_posts() ) {
										while ( have_posts() ) {
										 the_post();  ?>
										<?php the_content(); ?>
										 <?php } }  ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="post_wrapper">
						<div class="head_post">
							<h3>Recent Post</h3>
						</div>
						<ul class="post_listing">
							<?php   

							$args = array(

								'post_type' => 'post',
								
								'post_status' => 'publish',

								'posts_per_page' => 3,
								
								); 
								$my_posts = get_posts( $args );

								foreach($my_posts as $my_post){ 
							 
									$format = "F j, Y";
									$pfx_date = get_the_date( $format, $my_post->ID );
								?>

									<li>
										<a href="<?php echo get_permalink( $my_post->ID); ?>" title="<?php echo $my_post->post_title; ?>"><?php echo $my_post->post_title; ?></a>
										<small class="post_date"> <?php echo $pfx_date; ?> </small>
									</li>
								<?php } ?>
							
						</ul>
						<div class="head_post archive">
							<h3>Subscribe Here</h3>
						</div>
						
							<?php if ( is_active_sidebar( 'sidebar-33' ) ) : ?>
								
						<?php dynamic_sidebar( 'sidebar-33' ); ?>
								
						<?php endif; ?>
						
						<div id="subscription">
						<?php echo do_shortcode('[contact-form-7 id="21" title="Subscribe Form"]'); ?>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<div class="clearfix"></div>
<?php get_footer();
