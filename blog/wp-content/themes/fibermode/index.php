<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();?>

<div class="about_bg text-center">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="heading">
					<h1>Blog</h1>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>

<div class="blog_wrapper">
	<div class="container">
		<?php
			if ( have_posts() ) :
				$i=1;?>
				<div class="row">
				<?php 
				/* Start the Loop */
				while ( have_posts() ) : the_post();
				?>
					<div class="col-md-6 " id="blog_list<?php echo $i; ?>">

						<?php /*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'template-parts/post/content', get_post_format() );
						?>
					
				<?php $i++;endwhile; ?>
					</div>
		

				<div class="row">
					<div class="col-md-12">
						<div class="col-md-12 text-center pagination_container">
							<nav aria-label="Page navigation example">
								<ul class="pagination">
									<li class="page-item">
										<?php
				
										the_posts_pagination( array(
											'prev_text' => __( '&laquo', 'textdomain' ),
											'next_text' => __( '&raquo', 'textdomain' ),
										) );

										?>
				
									</li>
								</ul>
							</nav>
						</div>
					</div>
				</div>

			<?php else :

				get_template_part( 'template-parts/post/content', 'none' );

			endif;
			?>

		</div>
</div>
<div class="clearfix"></div>
<?php get_footer();
