<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<?php 
	set_post_thumbnail_size( 600, 400, true );
?>
	<div class="w-blog-post-h">
		<a href="<?php echo get_permalink(); ?>">
			<div class="w-blog-post-preview">
				<?php the_post_thumbnail(); ?> 
			</div>
		</a>
		<div class="w-blog-post-body">
			<h2 class="w-blog-post-title">
				<a class="entry-title" href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a>
			</h2>
			<div class="w-blog-post-meta">
				<time class="w-blog-post-meta-date">
					<?php if ( 'post' === get_post_type() ) {
							if ( is_single() ) {
								twentyseventeen_posted_on();
							} else {
								echo twentyseventeen_time_link();
							};
					};
					?>
				</time>
				<div class="w-blog-post-content">
				<?php
								if ( ! has_excerpt(get_the_ID()) ) {
									$excerpt=wp_trim_words( get_the_content(), 30, '...' );
								} else { 
									$excerpt=get_the_excerpt();
								}
							?>
								<p><?php echo $excerpt; ?></p>
				
					<a href="<?php echo get_permalink(); ?>" class="btn btn-read">Read More..</a>
				</div>
			</div>
		</div>
	</div>
</div>
