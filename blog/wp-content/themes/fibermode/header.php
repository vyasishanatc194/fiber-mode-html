<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<?php $fiber_html_url='http://www.fibermode.com'; ?>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta name = "title" content = "Custom Single & Multimode Fiber Optic Cables Suppliers | FiberMode">
		<meta name="description" content="FiberMode stocks the largest selection of custom high quality Singlemode and Multimode fiber optic cables. Customize the length and color of any of our durable cables.">
		<meta name="google-site-verification" content="xy6WjHxDtorOf_uPge8vdHSd6XYpDyn1VHO36eIqka0" />
		
				<title><?php echo wp_title('', true,''); ?> </title>
			
		<link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
		<link href="<?php bloginfo('template_url'); ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
		<link href="<?php bloginfo('template_url'); ?>/css/animate.css" rel="stylesheet" type="text/css"/>
		<link href="<?php bloginfo('template_url'); ?>/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
		<link href="<?php bloginfo('template_url'); ?>/css/style.css" rel="stylesheet" type="text/css"/>
		<link href="<?php bloginfo('template_url'); ?>/css/developer.css" rel="stylesheet" type="text/css"/>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108497822-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-108497822-1');
		</script>
		<?php wp_head(); ?>
	</head>
	<body>
		<div class="container_content inner_shadow">
			<header>
				<div class="container">
					<div class="row">
						<nav class="navbar">
							<div class="col-md-8 col-sm-12">
								<div class="navbar-header">
									<a class="navbar-brand" href="<?php echo $fiber_html_url; ?>/index.html"><img src="<?php bloginfo('template_url'); ?>/images/logo.gif" alt="brand_icon"/></a>
								</div>
								<div class="menu navbar-header">
									<button class="navbar-toggle collapsed" aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" type="button">
										<i class="fa fa-bars"></i>
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
									<div  id="navbar" class="collapse navbar-collapse" aria-expanded="false"> 
										<ul class="nav navbar-nav">
											<li><a href="<?php echo $fiber_html_url; ?>/product-listing-page-1.html">Products</a></li>
											<li><a href="<?php echo $fiber_html_url; ?>/about.html">Company</a></li>
											<li><a href="<?php echo $fiber_html_url; ?>/contact.html">Contact Us</a></li>
											
										</ul>
										<?php 
											 $defaults = array(
											  'theme_location'  => 'primary',
											  'menu'            => 'header-menu',
											  'container'       => 'ul',
											  'container_class' => '',
											  'container_id'    => '',
											  'menu_class'      => 'nav navbar-nav',
											  'menu_id'         => '',
											  'echo'            => true,
											  'fallback_cb'     => 'wp_page_menu',
											  'before'          => '',
											  'after'           => '',
											  'link_before'     => '',
											  'link_after'      => '',
											  'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
											  'depth'           => 0,
											  'walker'          => ''
											 );
											 wp_nav_menu( $defaults );
										?>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-12">
								<div class="email_link">
									
									
								</div>
								<div class="upper_social_links social_links">
									<ul>
									
										 <li>
											<a href="mailto:<?php echo get_settings('gmail_url'); ?>"><i class="fa fa-envelope-o"></i><?php echo get_settings('gmail_url'); ?></a>
										 </li> 										
									</ul>
								</div>
								
							</div>
						</nav>
					</div>
				</div>
			</header>
			<div class="clearfix"></div>