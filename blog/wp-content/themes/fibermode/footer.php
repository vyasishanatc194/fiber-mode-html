<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<?php $fiber_html_url='http://www.fibermode.com'; ?>
		<footer>
				<div class="container">
					<div class="row">
						<div class="upper_footer">
							<div class="col-md-8 col-sm-7">
								<div class="footer_links blog_links">
									<ul>
										<li><a href="<?php echo $fiber_html_url; ?>/product-listing-page-1.html">Products</a></li>
										<li><a href="<?php echo $fiber_html_url; ?>/about.html">Company</a></li>
										<li><a href="<?php echo $fiber_html_url; ?>/contact.html">Contact Us</a></li>
										<li><a href="<?php echo site_url(); ?>">Blog</a></li>
										
									</ul>
									
										
								</div>
							</div>
							<div class="col-md-4 col-sm-5">
								<div class="email_link footer_email">
									<a href="mailto:<?php echo get_settings('gmail_url'); ?>"><i class="fa fa-envelope-o"></i><?php echo get_settings('gmail_url'); ?></a>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="border">
							<div class="col-md-3 col-xs-12">
								<a class="navbar-brand" href="<?php echo $fiber_html_url; ?>/index.html">
									<img src="<?php bloginfo('template_url'); ?>/images/logo.gif" alt="brand_icon">
								</a>
							</div>
							<div class="col-md-5 col-xs-12">
								<div class="copyrights">
									<p><?php echo get_settings('footer_text'); ?></p>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</footer>
		</div>
		<script src="<?php bloginfo('template_url'); ?>/js/jquery-1.12.4.min.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/jquery-ui.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.mixitup.min.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.easing.1.3.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/validation.js"></script>
	</body>
	<?php wp_footer(); ?>
</html>