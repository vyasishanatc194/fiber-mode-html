<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<?php

if (!empty($id)) {
    $grid_success_service = new WP_Query(array(
        'order' => 'DESC',
        'orderby'  => 'menu order ID',
        'p' => $id,
        'post_type' => 'post',
        'post_status' => null,
        'posts_per_page' => 6,
		'paged' => get_query_var('paged') ? get_query_var('paged') : 1));
		
} else {
    $grid_success_service = new WP_Query(array(
        'order' => 'DESC',
        'orderby'  => 'menu order ID',
        'post_type' => 'post',
        'post_status' => null,
        'posts_per_page' => 6,
		'paged' => get_query_var('paged') ? get_query_var('paged') : 1));
}
?>
<div class="about_bg text-center">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="heading">
					<h1>Blog</h1>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="blog_wrapper">
	<div class="container">
		<div class="row">
			<?php 
				if(count($grid_success_service->have_posts()) > 0){
				$i=1;
				while ($grid_success_service->have_posts()) {
					$grid_success_service->the_post();
					set_post_thumbnail_size( 600, 400, true );
					$format = "F j, Y";
					$pfx_date = get_the_date( $format, get_the_ID() );
					$content = get_the_content();					

			?>
				<div class="col-md-6 " id="blog_list<?php echo $i; ?>">
					<div class="w-blog-post-h">
						<a href="<?php echo get_permalink(); ?>">
							<div class="w-blog-post-preview">
								<?php the_post_thumbnail(); ?> 
							</div>
						</a>
						<div class="w-blog-post-body">
							<h2 class="w-blog-post-title">
								<a class="entry-title" href="<?php echo get_permalink(); ?>"><?php echo the_title(); ?></a>
							</h2>
							<div class="w-blog-post-meta">
								<time class="w-blog-post-meta-date"><?php echo $pfx_date; ?></time>
								<div class="w-blog-post-content">
									<p><?php echo the_excerpt(); ?></p>
									<a href="<?php echo get_permalink(); ?>" class="btn btn-read">Read More..</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php  $i++; } }?>
						
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-12 text-center pagination_container">
					<nav aria-label="Page navigation example">
						<ul class="pagination">
							<li class="page-item">
								<?php
				
									$total_pages = $grid_success_service->max_num_pages;
									if ($total_pages > 1){
										$current_page = max(1, get_query_var('paged'));
										echo paginate_links(array(
										'base' => get_pagenum_link(1) . '%_%',
										'format' => '/page/%#%',
										'current' => $current_page,
										'total' => $total_pages,
										'prev_text'    => __('&laquo'),
										'next_text'    => __('&raquo'),
										));
									}
								?>    
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<?php get_footer();
